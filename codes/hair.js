var canvas  = document.getElementById('canvas');
  var context = canvas.getContext('2d');
  var webcam = document.getElementById('webcam');
  var hairs = [];
  $("#bb2").hide();
  $("#bby").hide();
  $("#bbn").hide();
  $("#phjo").hide();
  $("#ph").hide();
  $("#pphjo").hide();
  $("#pph").hide();
  $("#pt").hide();
  $("#lh").hide();
  
        // Wait for page to be ready
  window.addEventListener("load", function(e) {
  alert("Tervetuloa kokeilemaan KampausAR:n prototyyppiä.\nJos tulokset heittelevät vahvasti todellisesta, niin tarkistathan, \nettä vääristymä ei johdu saman värisestä taustasta tai vaatteista\n ja että valaisu on tasaista ja riittävää.\nPrototyyppi toimii tällä hetkellä parhaiten mobiililaitteilla pystysuunnassa!");
  console.log("Page loaded!");
  document.querySelector('#wrongColor').addEventListener('click', function (e) {
            window.location.reload(false);
        })
  
  // Store the color to track
  var color = {r: 255, g: 0, b: 0};

  var tolerance = 11;
  var swatch = document.getElementById("color");
  
  // Register custom color. Do we need more than one?
  tracking.ColorTracker.registerColor('dynamic', function(r, g, b) {
    return getColorDistance(color, {r: r, g: g, b: b}) < tolerance
  });
  
  // Create color tracker
  var tracker = new tracking.ColorTracker("dynamic");
  
  // Track event callback
  tracker.on('track', function(e) {
  
    context.clearRect(0, 0, canvas.width, canvas.height);
  
    if (e.data.length !== 0) {
  
      e.data.forEach(function(rect) {
        console.log(rect.x, rect.y);
        drawRect(rect, context, color);

        setTimeout(function () {
            trackingTask.stop();
        }, 1000);
        
        // Event listener for starting analyzing process
        document.querySelector('#rightColor').addEventListener('click', function (e) {
            findAreas();
            setTimeout(function () {
          $("#bby").hide();
          //$("#bbn").hide();
          $("#bb2").show();
        }, 2000);
        });

      // areas based to locations
      function findAreas() {

        var Lside = rect.x > 168 && rect.x < 263;
        var Rside = rect.x < 520 && rect.x > 600;
        var Tvert =  rect.y > 234 && rect.y < 353;
        var Mvert = rect.y > 483 && rect.y < 650 && rect.x > 168 && rect.x < 520;
        var Dvert = rect.y > 701 && rect.y < 984;
        var fringe = rect.y > 360 && rect.y < 440 && rect.x > 324 && rect.x < 433;

      if(Lside == true || Rside == true)
        {
          hairs.push("a");
        } 
        if(Tvert){
        hairs.push("c");
        } 
        if(Mvert & (Lside || Rside)) {
          hairs.push("d");
        }
        if(Dvert & (Lside || Rside)){
          hairs.push("e");
        } 
        if (fringe){
          hairs.push("f");
        }
        }
          
      });
  
    }

  });

  function removeDuplicates(arr){
        let unique_array = arr.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
        });
        return unique_array
        }
  

  function MakeAnalyze(){
        
        console.log(removeDuplicates(hairs));
        
        if(hairs.includes('c') && hairs.includes('d') && hairs.includes('e') && hairs.includes('f')){
        console.log("pitkät ja otsis");
        $("#phjo").show();
        } else if(hairs.includes('c') && hairs.includes('d') && hairs.includes('e')){
        console.log("pitkät");
        $("#ph").show();
 
        } else if(hairs.includes('c') && hairs.includes('d') && hairs.includes('f')){
        console.log("puoli & otsis");
        $("#pphjo").show();
      
        } else if(hairs.includes('c') && hairs.includes('d')){
          console.log("Tyylikäs puolipitkä tukka");
          $("#pph").show();
     
        } else if(hairs.includes('c') && hairs.includes('f')){
          console.log("Lyhyet hiukset ja otsatukka");
          $("#pt").show();
          
        } else if(hairs.includes('c')){
          console.log("Lyhyet hiukset, onkohan ponnari");
          $("#lh").show();
          
        } else {
          console.log("hups! Testi epäonnistui koetapa uudelleen.");
          alert("hups! Testi epäonnistui koetapa uudelleen.\nTähän voidaan tarjota myös kaljua, ellei sille anneta muita arvoja");
          if(alert = true) {
            window.location.reload(false);
          }
        }
        }
        
     // Make analyze for get feedback to user
     document.querySelector('#AnalyzeHairs').addEventListener('click', function (e) {
      MakeAnalyze();
      $("#bb2").hide();
    });
  
  // Start tracking
  const trackingTask = tracking.track(webcam, tracker, { camera: true } );
  trackingTask.run();
  // Add listener for the click event on the video
  // Pick color from location. We can use webcam listener, if we want player can just touch screen
  webcam.addEventListener("click", function (e) {
    // Grab color from the video feed where the click occured
    var c = getColorAt(webcam, e.offsetX, e.offsetY);
  
    // Update target color
    color.r = c.r;
    color.g = c.g;
    color.b = c.b;
  
    // Update the div's background so we can see which color was selected
    swatch.style.backgroundColor = "rgb(" + c.r + ", " + c.g + ", " + c.b + ")";

    setTimeout(function () {
          $("#bby").show();
          $("#bbn").show();
         
        }, 3000);
  });

  });
  
  // Calculates the Euclidian distance
  function getColorDistance(target, actual) {
  return Math.sqrt(
    (target.r - actual.r) * (target.r - actual.r) +
    (target.g - actual.g) * (target.g - actual.g) +
    (target.b - actual.b) * (target.b - actual.b)
  );
  }
  
  // Returns the color at the specified x/y location
  function getColorAt(webcam, x, y) {
  canvas.width = webcam.width;
  canvas.height = webcam.height;
  context.drawImage(webcam, 0, 0, webcam.width, webcam.height);
  
  var xloca = webcam.x;
  var yloca = webcam.y;
  xloca = 350;
  //xloca = 384;
  yloca = 290;
  
  var pixel = context.getImageData(xloca, yloca, 1, 1).data;
  return {r: pixel[0], g: pixel[1], b: pixel[2]};
  }
  
  // Draw a colored rectangle on the canvas
  function drawRect(rect, context, color) {
  /*
  context.strokeStyle = "rgb(" + color.r + ", " + color.g + ", " + color.b + ")";
  context.strokeRect(rect.x, rect.y, rect.width, rect.height);
  context.font = '11px Helvetica';
  context.fillStyle = "#fff";
  context.fillText('x: ' + rect.x + 'px', rect.x + rect.width + 5, rect.y + 11);
  context.fillText('y: ' + rect.y + 'px', rect.x + rect.width + 5, rect.y + 22);
  */
  //for ready one, we can use that version below. But we should keep location values, for mobile testing.
  
  context.fillRect(rect.x, rect.y, 10, 10);
  //context.fillStyle = "rgb(" + color.r + ", " + color.g + ", " + color.b + ")";
  context.fillStyle = "#7CFC00";
  
  }
